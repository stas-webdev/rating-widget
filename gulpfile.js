const gulp = require('gulp');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const path = require('path');

const srcDir = path.join(__dirname, 'src');
const assetsDir = path.join(__dirname, 'assets');
const distDir = path.join(__dirname, 'dist');

gulp.task('default', ['pug', 'sass', 'assets']);

gulp.task('watch', () => {
  return gulp.watch(srcDir + '/**/*.*', ['default']);
});

gulp.task('pug', () => {
  return gulp.src(srcDir + '/*.pug')
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest(distDir));
});

gulp.task('sass', () => {
  return gulp.src(srcDir + '/*.sass')
    .pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
    .pipe(gulp.dest(distDir));
});

gulp.task('assets', () => {
  return gulp.src(assetsDir + '/**/*')
    .pipe(gulp.dest(distDir));
});
